<?php
/**
 * Plugin Name:     WP-No-Auto-Origin-Header
 * Plugin URI:      https://burobjorn.nl
 * Description:     Prevent WordPress from automatically using the Origin header in the Access-Control-Allow-Origin header in the REST API. Use a safe-list of HTTP Origins (URLs) instead
 * Author:          Bj&ouml;rn Wijers <burobjorn@burobjorn.nl>
 * Author URI:      https://burobjorn.nl
 * Version:         1.0.0.
 *
 * @package         Wp_No_Auto_Origin_Header
 */

if( ! class_exists('WP_No_Auto_Origin_Header') ) {
  class WP_No_Auto_Origin_Header {
     
    /** 
     * Constructor, intentional left blank
     */
    public function __construct() {}

    /**
     * Setup the WordPress hooks and filters 
     */  
    public function setup_hooks_filters() {
      // Use the 'rest_api_init' hook to remove the filter, a standard remove_filter
      // dit not work.
      // Reference: 
      // https://joshpress.net/access-control-headers-for-the-wordpress-rest-api/
      add_action( 'rest_api_init', array( $this, 'remove_default_send_cors_headers' ) );

      // add the string 'null' to the allowed Origins for backwards-compatibility. 
      // It is used by file:// and data: URLS
      // and is currently used by the default WordPress REST API.
      // Reference: 
      // https://core.trac.wordpress.org/ticket/40011
      add_filter( 'allowed_http_origins', array( $this, 'add_null_to_allowed_http_origins' ) );  

      // Enable more secure CORS function utilizing the Allowed Origin API
      // Reference: 
      // https://core.trac.wordpress.org/changeset/20794
      add_filter( 'rest_pre_serve_request', array( $this, 'enable_allowed_cors_headers' ) ); 
    }
    
    /**  
     * Disable the less secure CORS headers in WordPress Core Rest API. 
     * It automatically adds any Origin to the Access-Control-Allow-Origin header
     * and does not use the 'allowed_http_origins' from the Allowed Origin API
     * 
     * Reference: 
     * https://core.trac.wordpress.org/browser/trunk/src/wp-includes/rest-api.php#L536 
     * https://core.trac.wordpress.org/browser/trunk/src/wp-includes/rest-api.php#L544
     */ 
    public function remove_default_send_cors_headers() {
      remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' ); 
    } 

    /** 
     * Add 'null' to the URL's set in get_http_allowed_origins() function using the
     * 'allowed_http_origins' filter. This allows requests from file:// and data: urls
     * to be accepted.
     *
     * References: 
     * https://core.trac.wordpress.org/browser/trunk/src/wp-includes/http.php#L422
     * https://core.trac.wordpress.org/browser/trunk/src/wp-includes/http.php#L449
     *
     * @param array urls of allowed origins
     * @return array urls of allowed origins including the string 'null'
     */
    public function add_null_to_allowed_http_origins( $allowed_origins ) {
      if( is_array( $allowed_origins) ) {
        $allowed_origins[] = 'null'; 
       }
      return $allowed_origins; 
    }


    /** 
     * Customized version of rest_send_cors_headers() function found in rest-api.php
     * This version uses the Allowed Origin API to verify the supplied Origin is part of the
     * allowed origins array set in the get_allowed_http_origins() function. 
     *
     * The set of allowed Origins can be easily set using the 'allowed_http_origins' filter
     * as I did by adding the 'null' string in the function add_null_to_allowed_http_origins()
     * of this plugin.
     *
     * If the supplied origin is allowed: set the headers, otherwise don't (fails silent)
     * 
     * @param mixed $value Response data.
     * @return mixed Response data.  
     */
    public function enable_allowed_cors_headers( $value ) {
      $origin = get_http_origin();
      $allowed_origins = get_allowed_http_origins();
      if ( $origin && in_array( $origin, $allowed_origins ) ) {
        // Requests from file:// and data: URLs send "Origin: null"
        if ( 'null' !== $origin ) {
          $origin = esc_url_raw( $origin );
        }

        header( 'Access-Control-Allow-Origin: ' . $origin );
        header( 'Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT, PATCH, DELETE' );
        header( 'Access-Control-Allow-Credentials: true' );
        header( 'Vary: Origin' );
      }
      return $value;
    } 
  }
  $wp_no_auto_origin_header = new WP_No_Auto_Origin_Header; 
  $wp_no_auto_origin_header->setup_hooks_filters(); 
} else {
  error_log( 'Could not initialize WP-No-Auto-Origin-Header plugin. Class WP_No_Auto_Origin_Header exists already' ); 
}
