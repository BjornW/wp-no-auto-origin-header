=== WP-No-Auto-Origin-Header ===
Contributors: BjornW
Tags: security, rest-api
Requires at least: 4.9
Tested up to: 4.9.6
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Prevent Wordpress from using the incoming Origin header in the Access-Control-Allow-Origin header in the REST API 

== Description ==

Prevent WordPress from using the incoming Origin header in the Acces-Control-Allow-Origin in the REST API. 
This should make WordPress less suspectible to CORS attacks. The plugin has no settings.


== Installation ==

1. Upload `wp-no-auto-origin-header` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Screenshots ==

no screenshots

== Changelog ==

= 1.0.0 November 27 2018 =   
  First release

